import pandas as pd
import datetime
import pandas_datareader as pdr #remote data access for pandas
import matplotlib.pyplot as plt
from matplotlib import style

def get_requirements():
    print("Data Analysis 1 \n")
    print("1. Run demo.py")
    print("2. If errors, more than likely missing installations.")
    print("3. Test Python Package Installer: Pip Freeze")
    print("4. Research how to do the following installations:")
    print("\t a. pandas (only if missing)")
    print("\t b. pandas-datareader (only if missing)")
    print("\t c. matplotlib (only if missing)")
    print("5. Create at least three functions that are called by the program: ")
    print("\t a. main(): calls at least two other functions.")
    print("\t b. get_requirements(): displays the program requirements.")
    print("\t c. data_analysis_1(): displays the following data.")

def data_analysis_1():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 15)

    #Note: XOM is stock market symbol for Exxon MObile Corp.
    df = pdr.DataReader("XOM", "yahoo", start,end)

    print("\nPrint number of records")
    #statement goes here
    print(df.shape[0])
    print("")

    #Why is it important to run the following print statement..
    print(df.columns)

    print("\n Print data frame: ")
    print(df) #Note: for efficiency, only prints 60-- not *all* records

    print("\n Print first five lines ")
    #Note: "date" is lower than the other columns as it is treated as an index
    print(df.head())

    print("\n Print last five lines")
    #statement goes here
    print(df.tail())

    print("\n Print first 2 lines:")
    #Statement goes here
    print(df.head(2))

    print("\n Print last 2 lines:")
    print(df.tail(2))

    #Research what these styles do 
    #Style.use 
    #compare with
    
    print("\n")
    print(plt.style.available)
    print("\n")
    plt.style.use('fivethirtyeight')
    df['High'].plot()
    df['Adj Close'].plot()

    plt.legend()
    plt.show()
