def get_requirements():
    print("Painting Estimator\n")
    print("Program Requirements:")
    print("1. Calculate home interior paint cost (w/o primer).")
    print("2. Must use float data types.")
    print("3. Must use SQFT_PER_GALLON constant (350).")
    print("4. Must use iteration structure (loop)")
    print("5. Format, right align numbers, and round to two decimal places")
    print("6. Create at least three functions that are called by the program:")
    print("\t a. main(): calls at least two other functions.")
    print("\t b. get_requirements(): displays that program requirements.")
    print("\t c. estimate_painting_cost(): calculates interior home painting.")
    

def estimate_painting_cost():
    responseYes = "y"
    response = "y"
    while response == responseYes:
        print("\n")
        print("Input:")
        totalInferiorSqFt = float(input("Enter total inferior sq. ft: \t"))
        pricePerGallonPaint = float(input("Enter price per gallon paint: \t"))
        hourlyPaintingRate = float(input("Enter hourly painting rate per sq. ft: \t"))
        SQFT_PER_GALLON = 350

        numOfGallons = totalInferiorSqFt / SQFT_PER_GALLON
        
        print("\n")
        print "Output:"
        print "Item", "Amount".rjust(26)
        print "Total Sq Ft:",  "{0:,.2f}\t" .format(totalInferiorSqFt).rjust(22)
        print "Sq Ft per Gallon: ", "{0:,.2f}".format(SQFT_PER_GALLON).rjust(13)
        print "Number of Gallons:", "{0:,.2f}\t" .format(numOfGallons).rjust(12)
        print "Paint per Gallon:", "${0:,.2f}\t" .format(pricePerGallonPaint).rjust(15)
        print "Labor per sq Ft:", "${0:,.2f}\t" .format(hourlyPaintingRate).rjust(15)
        
        paintCost = numOfGallons * pricePerGallonPaint
        laborCost = totalInferiorSqFt * hourlyPaintingRate
        totalCost = paintCost + laborCost
        paintPercentage = (paintCost / totalCost) * 100
        laborPercentage = (laborCost / totalCost) * 100
        totalPercentage = paintPercentage + laborPercentage
        print("\n")
        print "Cost", "Amount".rjust(14), "Percentage".rjust(25)
        print "Paint:", "${0:,.2f}" .format(paintCost).rjust(13), "{0:,.2f}%" .format(paintPercentage).rjust(20)
        print "Labor:", "${0:,.2f}" .format(laborCost).rjust(15), "{0:,.2f}%" .format(laborPercentage).rjust(19)
        print "Total:", "${0:,.2f}" .format(totalCost).rjust(15), "{0:,.2f}%" .format(totalPercentage).rjust(20)
        print("\n")
        response = raw_input("Estimate another paint job? (y/n):")
    
    print("\n")    
    print "Thank you for using our Painting Estimator!"
    print "Please see our web site: http://diegoleon.info"
    print("\n")
