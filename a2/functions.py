def get_requirements():
    print("Payroll Calculator\n")
    print("Program Requirements")
    print("1. Must use float data type for user input")
    print("2. Overtime rate: 1.5 times hourly rate (hours over 40)")
    print("3. Holiday rate: 2.0 times hourly rate (all holiday hours)")
    print("4. Must format currency with dollar sign, and round to two decimal places")
    print("5. Create at least three functions that are called by the program:")
    print("\t a. main(): calls at least two other functions.")
    print("\t b. get_requirements(): displays that program requirements.")
    print("\t c. calculate_payroll(): calculates an individual one-week paycheck.")
    

def calculate_payroll():
    print("User Input:")
    hoursWorked = float(input("Enter hours worked: \t"))
    holidayHours = float(input("Enter holiday hours: \t"))
    hourlyPayRate = float(input("Enter hourly pay rate: \t"))
    
    overtime = 0
    if hoursWorked > 40:
        overtime = hoursWorked - 40
        hoursWorked = hoursWorked - overtime 
    
    base = hoursWorked * hourlyPayRate
    overtime = overtime * (1.5 * hourlyPayRate)
    holidayHours = holidayHours  * (hourlyPayRate * 2)
    gross = base + overtime + holidayHours

    print("\n")
    print "Output:"
    print "Base: ${0:,.2f}\t" .format(base)
    print "Overtime: ${0:,.2f}\t" .format(overtime)
    print "Holiday: ${0:,.2f}\t" .format(holidayHours)
    print "Gross: ${0:,.2f}\t" .format(gross)