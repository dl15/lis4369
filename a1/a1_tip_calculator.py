print("Tip Calculator")
print("")
print("Program Requirements:")
print("1. Must use float data type for user input (except, \"Party Number).\"")
print("2. Must round calculations to two decimal places.")
print("3. Must format currency with dollar sign, and two decimal places.")
print("")
print("User Input:")

costOfMeal = float(input("Cost of meal: \t"))
taxPercent = float(input("Tax percent: \t"))
tipPercent = float(input("Tip percent: \t"))
partyNum = int(input("Party number: \t"))
print("")

print("Program Output: ")


taxForMeal = costOfMeal * (taxPercent/100)

print "Tax: \t \t ${:0.2f}".format(taxForMeal)

amountDueWithTax = taxForMeal + costOfMeal
gratuity = amountDueWithTax * (tipPercent/100)

total = gratuity + amountDueWithTax

print "Amount Due: \t ${0:,.2f}" .format(amountDueWithTax)
 
print "Gratuity: \t ${0:,.2f}" .format(gratuity)

print "Total: \t \t ${0:,.2f} " .format(total)

split = total / partyNum

print "Split (",partyNum,"):\t ${0:,.2f}".format(split)
