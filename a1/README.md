Git commands


1. git init: create an empty Git repository or reinitialize an existing one 

2. git status: show the working tree status

3. git add: add file contents to index

4. git commit: record changes to the repository

5. git push: update remote refs along with associated objects

6. git pull: fetch from and integrate with another repository or a local branch

7. git stash: stash the changes in a dirty working directory away


Screenshot of Microsoft visual studio running the A1_tip_Calculator application: 
![](images/screenshotVisualStudio.png "Screenshot of running application in Visual Studio")


Screenshot of IDLE running the A1_tip_Calculator application: 
![](images/screenshotPythonIDLE.png "Screenshot of running application in IDLE")


[Link to BitbucketStationLocations](https://bitbucket.org/dl15/bitbucketstationlocations/src/master/)