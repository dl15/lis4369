Screenshots of output analyzing Titanic passenger ages: 

![](images/screenshot1.png "")


![](images/screenshot2.png "")


Screenshots of two plots displaying the analysis:
![](images/screenshot3.png "Box plot plotted horizontally")


![](images/screenshot4.png "Scatter plot provides graphical view of relationship between two sets of numbers")

