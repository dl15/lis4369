Screenshots of output analyzing Motor Trend Car Road Tests data: 

![](images/screenshot1.png "")


![](images/screenshot2.png "")


Screenshots of two plots displaying the analysis:
![](images/screenshot3.png "qplot analyzing Displacement vs MPG")


![](images/screenshot4.png "plot analyzing Weight vs MPG")

