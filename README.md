# LIS4369 - Extensible Enterprise Solutions

## Diego Leon

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)

    * Install Python

    * Install R

    * Install Visual Studio Code

    * Create a1_tip_calculator application

    * provide screenshots of installations

    * Create Bitbucket Repo

    * Complete Bitbucket tutorial (bitbucketstationlocations)

    * Provide git command descriptions




2. [A2 README.md](a2/README.md)

    * Backward-engineer (Using Python) the following screenshots in a2

    *  Provide screenshots of application running




3. [A3 README.md](a3/README.md)

    * Develop a program that calculates how much a painting job would cost

    * Provide screenshots of application running




4. [A4 README.md](a4/README.md)

    * Use pandas-datareader to pull information on titanic passengers

    * Screenshot a graph based on passengers' ages streamed from the data reader

    * Call at least three functions


5.  [A5 README.md](a5/README.md)

    * Complete the Introduction to R Tutorial

    * Analyze data on the titanic passengers

    * Includes 2 plots and verify it is working using R Studio




6. [P1 README.md](p1/README.md)

    * Use pandas-datareader to display graphs with financial information

    * Screenshot a graph using the financial information streamed from the data reader

    * Call at least three functions 




7. [P2 README.md](p2/README.md)

    * Analyze Motor Trend Car Road Tests data

    * Display data using R Studio

    * Includes 2 plots and verify it is working using R Studio